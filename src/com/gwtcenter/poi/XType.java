package com.gwtcenter.poi;

import java.util.*;
import java.util.stream.*;

/**
 * Excelのデータ・タイプを表す
 * <p>
 * 拡張子、mimeタイプを定義する。
 * </p>
 * @author ysugimura
 */
public enum XType {
  /** 旧形式 */
  XLS("xls",  "application/vnd.ms-excel", XLimits.EXCEL97),
  
  /** 新形式 */
  XLSX("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet xlsx", XLimits.EXCEL2007);

  /** 各形式ファイル拡張子 */
  public final String extension;
  
  /** 表示用ラベル */
  public final String label;
  
  /** 各形式Mimeタイプ */
  public final String mimeType;
  
  /** 制限 */
  public final XLimits limits;
  
  private XType(String extension, String mimeType, XLimits limits) {
    this.extension = extension;
    this.label = extension.toUpperCase();
    this.mimeType = mimeType;
    this.limits = limits;
  }
  
  /** ラベル。ファイル拡張子を大文字としたもの */
  public static final String[]labels;
  static {
    labels = Arrays.stream(XType.values())
        .map(t->t.label + "形式")
        .collect(Collectors.toList()).toArray(new String[0]);
  }
}
