package com.gwtcenter.poi.read;

/**
 * Excelから読み込んだデータを表現する。
 * <p>
 * Excel、特にXLSX形式の場合、最終的にはXML文書として格納されており、これは文字列である。
 * この文字列をどう解釈するかは、別のデータによって行われる。数値なのか、文字列なのか、式なのか、論理値なのか。
 * 数値にも整数なのか浮動小数点数なのか、あるいは日付なのか、日付であれば、年月日なのか時刻までも含まれるのか。
 * </p>
 * <p>
 * これらについてユーザの期待通りに解釈するためには、元の文字列に付随する型や表示フォーマットを見なければならない。
 * これらから判断し、最も適切と思われるJavaの型に変換する。
 * </p>
 * <p>
 * 一つのセルデータを適切と思われるJavaとして表現するため、このクラスは少々無茶な構造になっている。
 * 任意のオブジェクトとそのデータ・タイプを格納しており、特定のデータ・タイプは特定の型のオブジェクトが格納される。
 * </p>
 * @author ysugimura
 */
public class XReadData {

  public final XReadType type;
  public final Object data;

  /** データを作成する。データ・タイプとオブジェクトが指定される */
  public XReadData(XReadType type, Object data) {
    if (data == null || type == null) 
      throw new NullPointerException();
    if (!type.clazz.isAssignableFrom(data.getClass()))
      throw new IllegalArgumentException("DataType mismatched");
    this.data = data;
    this.type = type;
  }

  /** データを任意の型に変換する */
  @SuppressWarnings("unchecked")
  public <T>T get() {
    return (T)data;
  }

  /**
   * 文字列化。デバッグ用
   */
  @Override
  public String toString() {
    return type + ":" + data;
  }
}
