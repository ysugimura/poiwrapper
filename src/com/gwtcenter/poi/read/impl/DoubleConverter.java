package com.gwtcenter.poi.read.impl;

import org.apache.poi.ss.usermodel.*;

import com.gwtcenter.poi.read.*;

public class DoubleConverter {

  /** Excel上でそのデータが「見られる」形にフォーマットする */
  private static DataFormatter dataFormatter = new DataFormatter();
  
  DoubleConverter() {
  }
  
  /**
   * Excelでは、たとえ整数でも数値は必ずdoubleとして格納されているが、フォーマットによって見かけの文字列が決められている。
   * ここで注力するのは、もし整数として表示されているのであれば、整数として取得すること。
   * そうでなければ、単純にdoubleを文字列化したものとして取得する。
   * @param cell 対象とするセル
   * @return セルの数値の文字列化
   */
  XReadData getDoubleData(Cell cell) {

    // セルのフォーマットに従って文字列化を行い、三桁カンマは除去する
    String s = dataFormatter.formatCellValue(cell).replace(",",  "");
    try {
      // 浮動小数点数値（整数を含む）として適正か。      
      double value = Double.parseDouble(s);
      if (s.indexOf('.') < 0) return new XReadData(XReadType.INT, (long)value);
      return new XReadData(XReadType.FLOAT, value);
    } catch (Exception ex) {
      // そうでなければ中身の数値を浮動小数点として返す。
      return new XReadData(XReadType.FLOAT, cell.getNumericCellValue());  
    }
  }
}
