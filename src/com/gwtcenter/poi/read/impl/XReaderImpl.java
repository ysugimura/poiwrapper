package com.gwtcenter.poi.read.impl;

import static java.util.stream.Collectors.*;

import java.io.*;
import java.util.*;
import java.util.stream.*;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import com.gwtcenter.poi.read.*;

/**
 * XLS,XLSXを読み込み、文字列配列の配列を返す。
 * @author ysugimura
 */
abstract class XReaderImpl implements Closeable, XReader {

  final InputStream in;
  final Workbook book;
  final DataFormat dataFormat;
  final DateConverter dateConverter;
  final DoubleConverter doubleConverter;
  XFormulaReadType formulaReadType = XFormulaReadType.NONE;
  
  /** 下位オブジェクトが{@link Workbook}を設定する */
  XReaderImpl(InputStream in, Workbook book) {
    this.in = in;
    this.book = book;    
    dataFormat = book.createDataFormat();
    dateConverter = new DateConverter(dataFormat);
    doubleConverter = new DoubleConverter();
  }

  /** {@inheritDoc} */
  @Override
  public String[] sheetNames() {
    int sheetCount = book.getNumberOfSheets();
    return IntStream.range(0, sheetCount).mapToObj(i -> book.getSheetName(i)).collect(Collectors.toList())
        .toArray(new String[0]);
  }

  static final String EMPTY = "";
  
  /** {@inheritDoc} */
  @Override
  public String[][] read(int index, XToString toString) {
    return Arrays.stream(readData(index)).map(row-> {
      return Arrays.stream(row).map(d->{
        if (d == null) return EMPTY;
        return toString.toString(d);
      }).collect(Collectors.toList()).toArray(new String[0]);
    }).collect(Collectors.toList()).toArray(new String[0][]);
  }
  
  /** {@inheritDoc} */
  @Override
  public XReadData[][] readData(int index) {
    
    // 指定されたシートを取得
    Sheet sheet = book.getSheetAt(index);
    
    // 開始行・終了行インデックスを取得
    int first = sheet.getFirstRowNum();
    int last = sheet.getLastRowNum();
    if (first < 0 || last< 0 || last < first) {
      return new XReadData[0][];
    }
    
    // 核行データを取得する
    List<XReadData[]>list = IntStream.rangeClosed(first, last)
      .mapToObj(rowIdx->sheet.getRow(rowIdx))
      .filter(rowObj->rowObj != null)
      .map(rowObj->getCells(rowObj))
      .collect(Collectors.toList());
    
    // 最終行（複数）が空であれば削除する    
    for (int i = list.size() - 1; i >= 0; i--) {
      if (list.get(i).length > 0) break; 
      list.remove(i);
    }
    
    // 配列に変換
    return list.toArray(new XReadData[0][]);
  }

  /** 指定行オブジェクトのすべてのセル値を取得する */
  private XReadData[]getCells(Row rowObj) {
    
    // 開始セルインデックス、終了セルインデックスを取得
    int first = rowObj.getFirstCellNum();
    int last = rowObj.getLastCellNum();
    if (first < 0 || last < 0 || last < first) {
      // データが全く無い場合は、通常first = -1, last = -1となる。
      return new XReadData[0];
    }
    
    // セル値を取得してリストとしてまとめる。値の無い場合にはnullが格納される。
    List<XReadData>list = IntStream.rangeClosed(first, last)
      .mapToObj(colIdx->rowObj.getCell(colIdx))
      .map(cell->cellToData(cell))
      .collect(Collectors.toList());
    
    // 無駄な最後のnull（連続）を削除する。
    for (int i = list.size() - 1; i >= 0; i--) {
      if (list.get(i) != null) break;
      list.remove(i);
    }

    // XReadDataの配列を返す。
    return list.stream().toArray(XReadData[]::new);
  }
  
  /** セルデータを{@link XReadData}に変換する */
  private XReadData cellToData(Cell cell) {
    if (cell == null) return null;
    switch (cell.getCellType()) {
    case STRING:
      // 文字列
      return new XReadData(XReadType.STRING, cell.getStringCellValue());
    case NUMERIC:
      // ただの数値か、あるいは日付の場合がある。
      if (DateUtil.isCellDateFormatted(cell)) {
        // 日付としてのフォーマットがされている
        return dateConverter.getDateData(cell);
      }
      // 日付ではない。数値
      return  doubleConverter.getDoubleData(cell);
    case FORMULA:
      // 式
      switch (formulaReadType) {
      case NONE:  // 読み込まない
        return null;
      case FORMULA:  // 式として読み込む
        return new XReadData(XReadType.FORMULA, cell.getCellFormula());
      default:  // 値を読み込む
        return getFormulaValue(cell);
      }
    case BOOLEAN:
      return new XReadData(XReadType.BOOLEAN, cell.getBooleanCellValue());
    default:
      return null;
    }    
  }
    
  private XReadData getFormulaValue(Cell cell) {
    Workbook book = cell.getSheet().getWorkbook();
    CreationHelper helper = book.getCreationHelper();
    FormulaEvaluator evaluator = helper.createFormulaEvaluator();
    CellValue cellValue = evaluator.evaluate(cell);
    switch (cellValue.getCellType()) {
    case BOOLEAN:
      return new XReadData(XReadType.BOOLEAN, cellValue.getBooleanValue());
    case NUMERIC:
      // ただの数値か、あるいは日付の場合がある。
      if (DateUtil.isCellDateFormatted(cell)) {
        // 日付としてのフォーマットがされている
        return dateConverter.getDateData(cell);
      }
      // 日付ではない。数値
      return  doubleConverter.getDoubleData(cell);
    case STRING:
      return new XReadData(XReadType.STRING, cellValue.getStringValue());
    default:
      return null;
    }
  }
  
  @Override
  public void close() throws IOException {
    in.close();
  }
  
  @Override
  public void setFormulaReadType(XFormulaReadType type) {
    this.formulaReadType = type;
  }
  
  /** XLS用のリーダー */
  public static class XLS extends XReaderImpl {
    public XLS(InputStream in) throws IOException {
      super(in, new HSSFWorkbook(in));
    }
  }

  /** XLSX用のリーダー */
  public static class XLSX extends XReaderImpl {
    public XLSX(InputStream in) throws IOException {
      super(in, new XSSFWorkbook(in));
    }
  }
}
