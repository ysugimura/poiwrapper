package com.gwtcenter.poi.read.impl;

import java.io.*;
import java.nio.file.*;

import com.gwtcenter.poi.*;
import com.gwtcenter.poi.read.*;

/**
 * XReaderを作成する
 * 
 * @author ysugimura
 */
public class XReaderCreator {

  /** 指定ファイルを読み込む{@link XReader}を作成する。種類はファイル名から判断する */
  public static XReader create(XType xType, Path path) throws IOException {
    String filename = path.getFileName().toString().toLowerCase();
    InputStream in = Files.newInputStream(path);
    try {
      return create(xType, in);
    } catch (IOException ex) {
      in.close();
      throw ex;
    }
  }

  /**
   * 指定種類のExcelを入力ストリームから読み込むオブジェクトを作成する。
   * 入力ストリームは{@link XReader#close()}呼び出し時に閉じられる。
   * 
   * @param type
   * @param in
   * @return
   * @throws IOException
   */
  public static XReader create(XType type, InputStream in) throws IOException {
    switch (type) {
    case XLS:
      return new XReaderImpl.XLS(in);
    case XLSX:
      return new XReaderImpl.XLSX(in);
    default:
      throw new RuntimeException("not supported");
    }
  }
}
