package com.gwtcenter.poi.read.impl;

import java.util.*;
import java.util.regex.*;

import org.apache.poi.ss.usermodel.*;

import com.gwtcenter.poi.read.*;

/**
 * セル値を適切な{@link XReadData}に変換する
 * <p>
 * 指定された日付{@link java.util.Date}オブジェクトが指定されたフォーマットで文字列としてExcel上では表示されている。
 * このとき、日付か、あるいは日付＋時刻の文字列として、「yyyy/MM/dd」あるいは「yyyy/MM/dd HH:mm:ss」形式の文字列を取得したい。
 * つまり、日付だけなのか時刻も合わせてなのかを判断する必要がある。
 * そこで、フォーマット文字列中の時刻を表す文字の有無を確認し、存在しなければ日付のみ、存在すれば時刻付とみなす。
 * </p>
 * <p>
 * </p>
 * @author ysugimura
 */
class DateConverter {
  
  /** Excel上に指定されたフォーマットに、「時刻」が含まれるかを判断するパターン */
  private static final Pattern DATE_PATTERN = Pattern.compile("^.*[YD].*$");
  
  /** Excel上に指定されたフォーマットに、「時刻」が含まれるかを判断するパターン */
  private static final Pattern TIME_PATTERN = Pattern.compile("^.*[HS].*$");

  private final Map<String, XReadType>formatMap = new HashMap<>();

  private final DataFormat dataFormat;
  
  DateConverter(DataFormat dataFormat) {
    this.dataFormat = dataFormat;
  }
  
  /**
   * {@link java.util.Date}を、Excel中で表示するためのフォーマットから日付あるいは日付＋時刻文字列に変換する。
   * @param date 入力データ
   * @param xlsFormat Excel中でのフォーマット文字列
   * @return 日付あるいは日付＋時刻文字列
   */
  XReadData getDateData(Cell cell) {
    
    Date date = cell.getDateCellValue();
    String xlsFormat = getFormatString(cell);
    
    switch (dateKind(xlsFormat)) {
    case DATE: return new XReadData(XReadType.DATE, date);
    case TIME: return new XReadData(XReadType.TIME, date);
    default: return new XReadData(XReadType.TIMESTAMP, date);
    }
  }
  
  /** Dateデータの形式から種類を得る */
  private XReadType dateKind(String xlsFormat) {
    {
      XReadType format = formatMap.get(xlsFormat);
      if (format != null) return format;
    }
    
    String upper = xlsFormat.toUpperCase();
    boolean hasDate = DATE_PATTERN.matcher(upper).matches();
    boolean hasTime = TIME_PATTERN.matcher(upper).matches();
    
    XReadType format = XReadType.DATE;
    if (hasDate && hasTime) format = XReadType.TIMESTAMP;
    else if (hasTime) format = XReadType.TIME;
    
    formatMap.put(xlsFormat, format);
    return format;
  }
  
  /** 指定セルのフォーマット文字列を取得する */
  private String getFormatString(Cell cell) {
    short number = cell.getCellStyle().getDataFormat();   
    return dataFormat.getFormat(number);
  }
}
