package com.gwtcenter.poi.read;

import java.io.*;

/**
 * XLS/XLSXを読み込むリーダー
 * <p>
 * 使用中はストリームがオープンされているので、使い終わったら{@link #close()}を呼び出すこと。
 * </p>
 * @author ysugimura
 */
public interface XReader extends Closeable {

  /** シート名を取得する */
  String[] sheetNames();

  /** 指定インデックスのシート内容を取得する */
  String[][] read(int index, XToString toString);
  
  /** XReadDataの二次元配列として取得する。{@link XReadData}参照のこと。 */
  XReadData[][]readData(int index);
  
  /** 式の読み込み形 式。デフォルトはNONE */
  void setFormulaReadType(XFormulaReadType type);
}