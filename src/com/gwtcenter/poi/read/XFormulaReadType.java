package com.gwtcenter.poi.read;

/**
 * セル中の式を読み込む場合の処理
 */
public enum XFormulaReadType {
  // 読み込まない
  NONE,
  // 式として読み込む
  FORMULA,
  // 値として読み込む。非常に遅い
  VALUE;
}
