package com.gwtcenter.poi.read;

import java.util.*;

/** 読込データ・タイプ */
public enum XReadType {
  INT(Long.class),
  FLOAT(Double.class),
  STRING(String.class),    
  DATE(Date.class),
  TIME(Date.class),
  TIMESTAMP(Date.class),
  FORMULA(String.class),
  BOOLEAN(Boolean.class);

  /** このデータ・タイプの場合に格納されるJavaの型 */
  public final Class<?>clazz;
  
  private XReadType(Class<?>clazz) {
    this.clazz = clazz;
  }
}