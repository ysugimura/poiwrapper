package com.gwtcenter.poi.read;

import java.text.*;
import java.util.*;

/**
 * {@link XReadData}を文字列化するコンバータ
 * @author ysugimura
 */
public class XToString {

  /**
   * 文字列化する
   * @param read 読込データ
   * @return 文字列
   */
  public String toString(XReadData read) {    
    switch (read.type) {
    case DATE: return convertDate(read.<Date>get());
    case TIMESTAMP: return convertTimestamp(read.<Date>get());
    case TIME: return convertTime(read.<Date>get());
    case FLOAT: return convertFloat(read.<Double>get());
    case STRING: return read.<String>get();
    case FORMULA: return read.<String>get();
    case BOOLEAN: return convertBoolean(read.<Boolean>get());
    case INT: return convertInt(read.<Long>get());
    default: throw new RuntimeException("Not supported:" + read.type);
    }
  }

  /** 整数を文字列化する */
  protected String convertInt(long value) {
    return "" + value;
  }
  
  /** 日付を文字列化する */
  protected String convertDate(Date value) {
    return DATE_ONLY.format(value);
  }

  /** タイムスタンプを文字列化する */
  protected String convertTimestamp(Date value) {
    return DATE_TIME.format(value);
  }

  /** 時刻を文字列化する */
  protected String convertTime(Date value) {
    return TIME_ONLY.format(value);
  }

  /** 浮動小数点数値を文字列化する */
  protected String convertFloat(double value) {
    return  String.format("%1$.2f", value);
  }

  /** 論理値を文字列化する */
  protected String convertBoolean(boolean value) {
    return "" + value;
  }
  
  /** 最終的に{@link java.util.Date}を日付文字列に変換するためのフォーマット */
  private static final SimpleDateFormat DATE_ONLY = new SimpleDateFormat("yyyy/MM/dd");

  /** 最終的に{@link java.util.Date}を日付＋時刻に変換するためのフォーマット */
  private static final SimpleDateFormat DATE_TIME = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

  /** 最終的に{@link java.util.Date}を時刻に変換するためのフォーマット */
  private static final SimpleDateFormat TIME_ONLY = new SimpleDateFormat("HH:mm:ss");

}
