package com.gwtcenter.poi.write;

import java.io.*;

import com.gwtcenter.poi.write.XFormat.*;

/**
 * Excelのワークブック
 * <p>
 * このオブジェクトは{@link Closeable}であり、close()すると、内部的なPOIのワークブックもclose()される。
 * さらに、SXSSFWorkbookの場合にはdispose()され、内部で使用された一時ファイルはすべて削除される。
 * </p>
 * @author ysugimura
 */
public interface XBook extends Closeable {

  /** セルスタイルを作成する */
  XCellStyle createCellStyle();
  
  /** フォントビルダーを作成する */
  XFont.Builder createFontBuilder();
  
  /** シートを作成する */
  XSheet createSheet(String name);

  /** 日付フォーマット取得 */
  DateFmt dateFormat(String format);

  /** タイムスタンプフォーマット取得 */
  TimestampFmt timestampFormat(String format);
  
  /** 文字列フォーマット取得 */
  StringFmt stringFormat(String format);

  /** 数値型フォーマット取得  */
  FloatFmt floatFormat(String format);

  /** 
   * 指定された出力ストリームにワークブックを書き込む。
   * 書き込み後に出力ストリームは閉じられる。
   * 上位でclose()動作の必要は無いが、close()しても害はない。
   */
  void write(OutputStream out) throws IOException;
}
