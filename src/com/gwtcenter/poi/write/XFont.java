package com.gwtcenter.poi.write;

public interface XFont {

  public interface Builder {
    public Builder setName(String name);
    public Builder setSize(int size);
    public Builder setItalic();
    public Builder setBold();
    public XFont build();
  }
}
