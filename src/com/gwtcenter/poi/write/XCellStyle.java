package com.gwtcenter.poi.write;

import com.gwtcenter.poi.write.XFmtStyle.*;
import com.gwtcenter.poi.write.XFormat.*;

/**
 * セルスタイル
 * <p>
 * 
 * </p>
 * @author ysugimura
 */
public interface XCellStyle {

  /** セルの枠位置を示す */
  public enum Side {
    TOP, BOTTOM, LEFT, RIGHT;
  }

  /** ボーダーの太さを示す */
  public enum Border {
    NONE, THIN, MEDIUM;
  }

  /** 指定枠位置に指定ボーダーを設定する */
  XCellStyle setBorder(Side side, Border boder);
  
  /** 全枠位置に細いボードを置く */
  XCellStyle setThinBorders();

  /** 背景色を設定する */
  XCellStyle setBgColor(XColor color);

  /** フォントを設定する */
  XCellStyle setFont(XFont font);
  
// https://www.gwtcenter.com/poi-horizontal-alignment
// を参照のこと。
//  /** 
//   * 水平アラインメントを指定する。0, 1, 2のいずれか 
//   * ※注意：これは使わない方がよい。セル内容による自然なアラインメントに任せた方がよい。
//   * @param value
//   * @return
//   */
//  @Deprecated
//  XCellStyle align(int value);

  //////////////////////////////////////////
  // 
  //////////////////////////////////////////
  
  /** 日付セル用のスタイルを作成する */
  DateStyle dateStyle(DateFmt format);

  /** タイムスタンプセル用のスタイルを作成する */
  TimestampStyle timestampStyle(TimestampFmt format);
  
  /** 文字列セル用のスタイルを作成する */
  StringStyle stringStyle(StringFmt format);

  IntStyle intStyle(IntFmt format);
  
  /** 数値セル用のスタイルを作成する */
  FloatStyle floatStyle(FloatFmt format);

  /** 論理値セル用のスタイルを作成する */
  BooleanStyle booleanStyle();
  
  /** 式セル用のスタイルを作成する */
  FormulaStyle formulaStyle(XFormat format);  
}