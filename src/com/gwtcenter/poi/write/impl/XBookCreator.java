package com.gwtcenter.poi.write.impl;

import java.io.*;
import java.nio.file.*;
import java.util.function.*;

import org.apache.poi.util.*;

import com.gwtcenter.poi.*;
import com.gwtcenter.poi.write.*;

/**
 * Excelワークブックの作成
 */
public class XBookCreator {
  
  /**
   * XLSX作成にはSXSSFWorkbookを使用しているが、これはメモリを圧迫しない代わりに一時ファイルを使うようになている。
   * このメソッドでサプライヤを提供すると、好みの場所に一時ファイルを作成させることができる。
   * @param supplier 一時ファイル提供サプライヤ
   */
  public static void setTempFileSupplier(Supplier<Path>supplier) {
    TempFile.setTempFileCreationStrategy(new DefaultTempFileCreationStrategy() {
      public File createTempFile(String prefix, String suffix) throws IOException {
        return supplier.get().toFile();
      }
    });    
  }

  /**
   * XBookの作成
   * 
   * {@link XType.XLSX}の場合には、一時ファイルが作成されることに注意。
   * これは{@link #setTempFileSupplier(Supplier)}によって場所を指定できる。
   * 作成された一時ファイルは{@lihk close()}時に自動的に削除される。
   * 
   * @param type タイプ、XLS, XLSX
   * @return {@link XBook}
   */
  public static XBook create(XType type) {
    return new XBookImpl(type);
  }
}
