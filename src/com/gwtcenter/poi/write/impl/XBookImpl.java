package com.gwtcenter.poi.write.impl;

import java.io.*;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.*;
import org.apache.poi.xssf.streaming.*;

import com.gwtcenter.poi.*;
import com.gwtcenter.poi.write.*;
import com.gwtcenter.poi.write.XFormat.*;

/**
 * {@link XBook}の実装
 * @author ysugimura
 */
class XBookImpl implements Closeable, XBook {

  protected Workbook workbook;
  
  /** データフォーマット */
  protected DataFormat dataFormat;
  
  private CellStyleMap cellStyleMap;
  
  XBookImpl(XType type) {    
    switch (type) {
    case XLS: workbook = new HSSFWorkbook(); break;
    case XLSX: workbook = new SXSSFWorkbook(); break;
    default:  throw new IllegalArgumentException("" + type);
    }
    dataFormat = workbook.createDataFormat();
    cellStyleMap = new CellStyleMap(workbook);
  }
  
  /** {@inheritDoc} */
  @Override
  public XFont.Builder createFontBuilder() {
    int number = cellStyleMap.fontList.size();
    Font font = workbook.createFont();
    cellStyleMap.fontList.add(font);
    return new XFontImpl.BuilderImpl(font, number);
  }
  
  /** {@inheritDoc} */
  @Override
  public XSheet createSheet(String nameProposal) {
    // シート名の文字として許されないものがあるため、チェックして変更
    String name = WorkbookUtil.createSafeSheetName(nameProposal);
    Sheet currentSheet = workbook.createSheet(name);
    return new XSheetImpl(this, currentSheet);
  }
  
  /** {@inheritDoc} */
  @Override
  public final DateFmt dateFormat(String format) {
    return new DateFmt(getFormat(format));
  }
  
  /** {@inheritDoc} */
  @Override
  public final TimestampFmt timestampFormat(String format) {
    return new TimestampFmt(getFormat(format));
  }
  
  /** {@inheritDoc} */
  @Override
  public final StringFmt stringFormat(String format) {
    return new StringFmt(getFormat(format));
  }
  
  /** {@inheritDoc} */
  @Override
  public final FloatFmt floatFormat(String format) {
    return new FloatFmt(getFormat(format));
  }

  @Override
  public XCellStyle createCellStyle() {
    return new XCellStyleImpl(cellStyleMap);
  }
  
  /** {@inheritDoc} */
  @Override
  public void write(OutputStream out) throws IOException {
    workbook.write(out);
    out.close();
  }
  
  /** 
   * フォーマット文字列からフォーマット番号を得る。
   * 同じフォーマット文字列について何度呼ばれても同じ番号を返す。
   * @param format
   * @return
   */
  short getFormat(String format) {
    short number = dataFormat.getFormat(format);    
    return number;
  }

  /** このワークブックラッパがclose()されたとき、ワークブックをclose()し、SXSSFWorkbookの場合はdispose()する
   */
  @Override
  public void close() throws IOException {
    workbook.close();
    if (workbook instanceof SXSSFWorkbook) {
      ((SXSSFWorkbook)workbook).dispose();
    }
  }

}