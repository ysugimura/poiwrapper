package com.gwtcenter.poi.write.impl;

import java.util.*;
import java.util.stream.*;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.*;
import org.apache.poi.xssf.streaming.*;

import com.gwtcenter.poi.write.*;
import com.gwtcenter.poi.write.XFmtStyle.*;
import com.gwtcenter.poi.write.impl.XCellImpl.*;

class XSheetImpl implements XSheet {

  final XBook book;
  final Sheet sheet;
  final RowMap rowMap = new RowMap();
  int currentRow = 0;
  int currentCol = 0;  
  int colCount = 0;
  int rowCount = 0;
  
  XSheetImpl(XBook book, Sheet sheet) {
    this.book = book;
    this.sheet = sheet;
    if (sheet instanceof  SXSSFSheet) {
      ((SXSSFSheet)sheet).trackAllColumnsForAutoSizing();
    }
  }
  
  @Override
  public void setRowHeight(float size) {
    rowMap.get(currentRow).setHeightInPoints(size);
  }
  
  @Override
  public XSheetImpl setHeader(int rows) {
    return setHeader(rows, 0);
  }

  @Override
  public XSheetImpl setHeader(int rows, int cols) {
    
    // 表示上で行あるいは列を固定する
    sheet.createFreezePane(cols, rows);

    // 印刷上で行を固定する。つまり、縦長の表を連続したページに印刷する場合に、どのページにも最初のrows分が現れる。
    if (rows > 0) {
      int first = 0;
      int last = rows - 1;
      try {
        sheet.setRepeatingRows(new CellRangeAddress(first, last, 0, colCount - 1)); 
      } catch (IllegalArgumentException ex) {
        System.err.println("setRepeatingRows error " + first + "," + last);
      }
    }

    // 印刷上で列を固定する。横長の表を連続したページに印刷する場合に、どのページにも最初のcols分が現れる。
    if (cols > 0) {
      int first = 0;
      int last = cols - 1;
      try {
        sheet.setRepeatingColumns(new CellRangeAddress(0, rowCount - 1, first, last));
      } catch (IllegalArgumentException ex) {
        System.err.println("setRepeatingColumns error " + first + "," + last);
      }
    }
    return this;
  }
  
  /** {@inheritDoc} */
  @Override
  public XSheetImpl position(int row, int col) {
    this.currentRow = row;
    this.currentCol = col;
    return this;
  }

  /** {@inheritDoc} */
  @Override
  public final void nextRow() {
    currentRow++;
    currentCol = 0;
  }

  /** {@inheritDoc} */
  @Override
  public final void skip() {
    currentCol++;
  }
    
  /** 現在位置にセルを作成する */
  public Cell createCell(CellType cellType) {
    try {
      rowCount = Math.max(rowCount, currentRow + 1);
      Cell cell = rowMap.get(currentRow).createCell(currentCol++);
      colCount = Math.max(currentCol, colCount);
      if (cellType != null)
        cell.setCellType(cellType);
      return cell;
    } catch (IllegalArgumentException ex) {
      // XLS形式で最大列数が256
      if (ex.getMessage().contains("Invalid column index")) {
        throw new IllegalArgumentException("XLS形式での最大列数は256です。XLSX形式をお使いください。", ex);
      }
      throw ex;
    }
  }
  
  /** {@inheritDoc} */
  @Override
  public void autoSizeAll() {
    try {
      IntStream.range(0, colCount).forEach(c-> {
        ensureAllRowsFilled(c);
        sheet.autoSizeColumn(c); 
      });
    } catch (RuntimeException ex) {
      System.err.println("" + ex.getMessage());
    }
  }
  
  @Override
  public void setRowBreak() {
    sheet.setRowBreak(currentRow);
  }
  
  /** {@inheritDoc} */
  @Override
  public void autoSize(int... columns) {
    try {
      IntStream.range(0, columns.length).forEach(i-> { 
        int column = columns[i];
        ensureAllRowsFilled(column);
        sheet.autoSizeColumn(column); 
      });
    } catch (RuntimeException ex) {
      System.err.println("" + ex.getMessage());
    }
  }

  /** 全行について、指定列のデータがあることを確実にする */
  private void ensureAllRowsFilled(int column) {
    try {
      rowMap.rows().forEach(row -> {
        if (row.getCell(column) != null)
          return;
        Cell cell = row.createCell(column);
        cell.setCellValue("");
      });
    } catch (RuntimeException ex) {
      System.err.println("" + ex.getMessage());
    }
  }
  
  class RowMap  {
    Map<Integer, Row>map = new HashMap<>();
    void put(int row, Row rowObject) {
      map.put(row, rowObject);
    }
    Row get(int row) {
      Row rowObject = map.get(row);
      if (rowObject != null) return rowObject;
      rowObject = sheet.createRow(row);
      map.put(row, rowObject);
      return rowObject;
    }
    Stream<Row>rows() {
      return map.values().stream();
    }
    String rowsString() {
      return map.keySet().stream().sorted().map(i->i.toString()).collect(Collectors.joining(","));
    }
  }


  @Override
  public IntCellImpl add(IntStyle style, long value) {
    return  new IntCellImpl(createCell(CellType.NUMERIC)).style(style).value(value);
  }

  @Override
  public FloatCellImpl add(FloatStyle style, double value) {
    return new FloatCellImpl(createCell(CellType.NUMERIC)).style(style).value(value);
  }

  @Override
  public StringCellImpl add(StringStyle style, String value) {
    return new StringCellImpl(createCell(CellType.STRING)).style(style).value(value);
  }

  @Override
  public DateCellImpl add(DateStyle style, Date value) {
    return new DateCellImpl(createCell(CellType.NUMERIC)).style(style).value(value);
  }

  @Override
  public TimestampCellImpl add(TimestampStyle style, Date value) {
    return new TimestampCellImpl(createCell(CellType.NUMERIC)).style(style).value(value);
  }
 
  @Override
  public FormulaCellImpl add(FormulaStyle style, String value) {
    return new FormulaCellImpl(createCell(CellType.FORMULA)).style(style).value(value);
  }
  
  @Override
  public BooleanCellImpl add(BooleanStyle style, boolean value) {
    return new BooleanCellImpl(createCell(CellType.BOOLEAN)).style(style).value(value);
  }
}
