package com.gwtcenter.poi.write.impl;

import java.util.*;

import org.apache.poi.ss.usermodel.*;

import com.gwtcenter.poi.write.*;
import com.gwtcenter.poi.write.XFmtStyle.*;
import com.gwtcenter.poi.write.XFormat.*;

/**
 * セル属性
 * <p>
 * 設定されるセル属性はすべて{@link CellStyleSpec}に格納され、{@link CellStyleMap}によって
 * 一つの{@link CellStyleSpec}から一つの{@link CellStyle}が作成される。
 * ここのような方法をとるのは、{@link CellStyle}の数に制限があるからで、「たまたま同じになった{@link CellStyleSpec}
 * については唯一の{@link CellStyle}を使うものとする。
 * </p>
 * <p>
 * 上記を実行するために、セル属性の指定の際は以下が行われる。
 * </p>
 * <ul>
 * <li>{@link CellStyleSpec}が複製される。これは{@link #dupSpec()}で行われる。
 * XCellStyleImplが使い回される際には、ShallowCopyが用いられているからである。
 * このため、複製せずに変更すると、他での使用にも影響してしまう。
 * <li>{@link CellStyleSpec}の状態を変更する。既に複製されているため、他への影響は無い。
 * <ul>もしこのオブジェクトから既にセルへの{@link CellStyle}が設定されていた場合には、再度セルに設定するため
 * {@link #resetCell()}が呼び出される。
 * </ul>
 * @author ysugimura
 */
class XCellStyleImpl implements XCellStyle  {
  
  final CellStyleMap cellStyleMap;
  
  CellStyleSpec spec = new CellStyleSpec();
  Cell cell;
  
  @Override
  public String toString() {
    return XCellStyleImpl.class.getSimpleName() + ":" + spec + "," + cell;
  }
  
  XCellStyleImpl(CellStyleMap cellStyleMap) {
    this.cellStyleMap = cellStyleMap;
  }

  XCellStyleImpl shallowCopy() {
    XCellStyleImpl impl = new XCellStyleImpl(cellStyleMap);
    impl.spec = this.spec;
    //impl.cell = this.cell; 元のセルを参照する必要はない
    return impl;
  }
  
  void setCell(Cell cell) {
    this.cell = cell;
    resetCell();
  }
  
  void setFormat(XFormat format) {
    spec = spec.setFormat(format.number);
    resetCell();
  }
  
  /** {@inheritDoc} */
  @Override
  public XCellStyle setThinBorders() {
    spec = spec.setBorderAll(Border.THIN);
    resetCell();
    return this;
  }  
  
  /** {@inheritDoc} */
  public XCellStyleImpl setBorder(Side side, Border border) {
    spec = spec.setBorder(side,  border);
    resetCell();
    return this;
  }

  /** {@inheritDoc} */
  @Override
  public XCellStyle setBgColor(XColor color) {        
    spec = spec.setBgColor(Optional.of(color));
    resetCell();
    return this;
  }

  /** {@inheritDoc} */
  @Override
  public XCellStyle setFont(XFont font) {
    spec = spec.setFont(Optional.of(((XFontImpl)font).number));
    resetCell();
    return this;
  }

  /**
   * このオブジェクトがセルへの属性設定を担当している場合、{@link CellStyleSpec}から
   * {@link CellStyle}を作成してセルに設定する。
   */
  private void resetCell() {
    if (cell == null) return;
    CellStyle cellStyle = cellStyleMap.get(spec);
    //ystem.out.println("align:" + cellStyle.getAlignment());
    cell.setCellStyle(cellStyle);
  }
  
  ////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////
  
  /** {@inheritDoc} */
  @Override
  public DateStyle dateStyle(DateFmt format) {
    XCellStyleImpl shallow = shallowCopy();
    shallow.setFormat(format);
    return new DateStyle(shallow);
  }

  /** {@inheritDoc} */
  @Override
  public TimestampStyle timestampStyle(TimestampFmt format) {
    XCellStyleImpl shallow = shallowCopy();
    shallow.setFormat(format);
    return new TimestampStyle(shallow);
  }
  
  /** {@inheritDoc} */
  @Override
  public StringStyle stringStyle(StringFmt format) {
    XCellStyleImpl shallow = shallowCopy();
    shallow.setFormat(format);
    return new StringStyle(shallow);
  }
  
  @Override
  public IntStyle intStyle(IntFmt format) {
    XCellStyleImpl shallow = shallowCopy();
    shallow.setFormat(format);
    return new IntStyle(shallow);
  }

  /** {@inheritDoc} */
  @Override
  public FloatStyle floatStyle(FloatFmt format) {
    XCellStyleImpl shallow = shallowCopy();
    shallow.setFormat(format);
    return new FloatStyle(shallow);
  }
  
  /** {@inheritDoc} */
  @Override
  public BooleanStyle booleanStyle() {
    XCellStyleImpl shallow = shallowCopy();
    return new BooleanStyle(shallow);
  }
  
  /** {@inheritDoc} */
  @Override
  public FormulaStyle formulaStyle(XFormat format) {
    XCellStyleImpl shallow = shallowCopy();
    shallow.setFormat(format);
    return new FormulaStyle(shallow);
  }
}
