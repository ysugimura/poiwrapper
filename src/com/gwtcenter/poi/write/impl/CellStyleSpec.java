package com.gwtcenter.poi.write.impl;

import java.util.*;
import java.util.stream.*;

import org.apache.poi.ss.usermodel.*;

import com.gwtcenter.poi.write.*;
import com.gwtcenter.poi.write.XCellStyle.*;

class CellStyleSpec  implements Cloneable {
  
  private Map<Side, Border>borderMap = new HashMap<>();
  
  CellStyleSpec setBorderAll(Border border) {
    CellStyleSpec cloned = clone();
    Arrays.stream(Side.values()).forEach(side->cloned.borderMap.put(side,  border));
    return cloned;
  }
  
  CellStyleSpec setBorder(Side side, Border border) {    
    CellStyleSpec cloned = clone();
    cloned.borderMap.put(side,  border);
    return cloned;
  }
  
  private Optional<XColor>bgColor = Optional.empty();  
  
  CellStyleSpec setBgColor(Optional<XColor>value) {
    CellStyleSpec cloned = clone();
    cloned.bgColor = value;
    return cloned;
  }
  
  private short format = 0;

  CellStyleSpec setFormat(short format) {
    CellStyleSpec cloned = clone();
    cloned.format = format;
    return cloned;
  }
  
  private Optional<Integer> font = Optional.empty();

  CellStyleSpec setFont(Optional<Integer>font) {
    CellStyleSpec cloned = clone();
    cloned.font = font;
    return cloned;
  }
  
  /** デバッグ用文字列化 */
  @Override
  public String toString() {
    return 
      "borderMap:" + borderMap.entrySet().stream().map(e->e.getKey() + "=" + e.getValue()).collect(Collectors.joining(",")) + "\n" +
      "bgColor:" + bgColor + "\n" +
      "format:" + format + "\n" +
      "font:" + font + "\n";      
  }
  
  public CellStyleSpec clone() {
    try {
      CellStyleSpec that = (CellStyleSpec)super.clone();
      that.borderMap = new HashMap<>(this.borderMap);
      return that;
    } catch (Exception ex) {
      throw new RuntimeException();
    }
  }
  
  @Override
  public int hashCode() {
    return 
        borderMap.hashCode() +
        bgColor.hashCode() +
        format +
        font.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof CellStyleSpec)) return false;
    CellStyleSpec that = (CellStyleSpec)o;
    return 
      this.borderMap.equals(that.borderMap) &&
      this.bgColor.equals(that.bgColor)&&
      this.format == that.format &&
      this.font.equals(that.font);
  }
  
  void setTo(CellStyle cellStyle, List<Font>fontList) {
    borderMap.entrySet().stream().forEach(e-> {
      setBorder(cellStyle, e.getKey(), e.getValue());
    });
    bgColor.ifPresent(c->bgColor(cellStyle, c));
    cellStyle.setDataFormat(format);
    font.ifPresent(f->cellStyle.setFont(fontList.get(f)));
  }
  
  private static Map<Border, BorderStyle>borderStyles = new HashMap<Border, BorderStyle>() {{
    put(Border.NONE, BorderStyle.NONE);
    put(Border.THIN, BorderStyle.THIN);
    put(Border.MEDIUM, BorderStyle.MEDIUM);
  }};
  
  private void setBorder(CellStyle cellStyle, Side side, Border border) {
    BorderStyle borderStyle = borderStyles.get(border);
    switch (side) { 
    case TOP:  cellStyle.setBorderTop(borderStyle); break;
    case BOTTOM: cellStyle.setBorderBottom(borderStyle); break;
    case LEFT:cellStyle.setBorderLeft(borderStyle); break;
    case RIGHT: cellStyle.setBorderRight(borderStyle); break;
    }
  }


  private void bgColor(CellStyle cellStyle, XColor color) {        
    cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    cellStyle.setFillForegroundColor(color.value);
  }

}
