package com.gwtcenter.poi.write.impl;

import org.apache.poi.ss.usermodel.*;

import com.gwtcenter.poi.write.*;

public class XFontImpl implements XFont {

  Font font;
  int number;
  XFontImpl(Font font, int number) {
    this.font = font;
    this.number = number;
  }
  
  public static class BuilderImpl implements XFont.Builder {
    final Font font;
    final int number;
    boolean built;
    BuilderImpl(Font font, int number) {
      this.font = font;
      this.number = number;
    }
    @Override
    public Builder setName(String name) {
      notBuilt();
      font.setFontName(name);
      return this;
    }
    @Override
    public Builder setSize(int size) {
      notBuilt();
      font.setFontHeightInPoints((short)size);
      return this;
    }
    @Override
    public Builder setItalic() {
      notBuilt();
      font.setItalic(true);
      return this;
    }
    @Override
    public Builder setBold() {
      notBuilt();
      font.setBold(true);
      return this;
    }
    public XFont build() {
      notBuilt();
      built = true;
      return new XFontImpl(font, number);
    }
    
    private void notBuilt() {
      if (built) throw new RuntimeException();
    }
  }
}
