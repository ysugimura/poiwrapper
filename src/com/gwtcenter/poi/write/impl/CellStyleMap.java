package com.gwtcenter.poi.write.impl;

import java.util.*;

import org.apache.poi.ss.usermodel.*;

/**
 * {@link CellStyleSpec}と{@link CellStyle}のマップ。
 * 特定の{@link CellStyleSpec}の状態から作成された{@link CellStyle}をマップとして保持する。
 * この理由はExcel上で保持できる{@link CellStyle}の数に制限があるので、できる限り{@link CellStyle｝の数をへらすため。
 * @author ysugimura
 */
class CellStyleMap {

  /** 新たな{@link CellStyle}の作成に{@link Workbook}が必要 */
  final Workbook workbook;
  
  /** {@link CellStyleSpec}/{@link CellStyle}のマップ */
  final Map<CellStyleSpec, CellStyle>map = new HashMap<>();
  
  /** 作成済のフォントリスト、このリストのインデックスがフォント番号になる */
  final List<Font>fontList = new ArrayList<>();
  
  CellStyleMap(Workbook workbook) {
    this.workbook = workbook;
  }
  
  /**
   * 指定された{@link CellStyleSpec}に相当する{@link CellStyle}を取得する。
   * @param spec
   * @return
   */
  CellStyle get(CellStyleSpec spec) {
    
    // 既に指定されたspecについてcellStyleがあればそれを返す。
    CellStyle cellStyle = map.get(spec);
    if (cellStyle != null) return cellStyle;
    
    // なければ新たにcellStyleを作成し、マップに登録してそれを返す。
    cellStyle = workbook.createCellStyle();
    spec.setTo(cellStyle, fontList);
    map.put(spec, cellStyle);
    return cellStyle;
  }
}
