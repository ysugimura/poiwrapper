package com.gwtcenter.poi.write.impl;
import java.util.*;

import org.apache.poi.ss.usermodel.*;

import com.gwtcenter.poi.write.*;
import com.gwtcenter.poi.write.XFmtStyle.*;

public class XCellImpl implements XCell {

  Cell cell;
  XCellStyleImpl cellStyle;
  
  protected XCellImpl(Cell cell) {
    this.cell = cell;
  }

  public XCellStyleImpl getStyle() {
    return cellStyle;
  }
  
  protected void style(XFmtStyle fmt) {
    cellStyle = ((XCellStyleImpl)fmt.cellStyle).shallowCopy();
    cellStyle.setCell(cell);
  }
  
  public static class IntCellImpl extends XCellImpl  {
    public IntCellImpl(Cell cell) {
      super(cell);
    }
    
    public IntCellImpl style(IntStyle style) {
      super.style(style);
      return this;
    }
    
    public IntCellImpl value(long value) {
      cell.setCellValue(value);
      return this;
    }
  }
  
  public static class FloatCellImpl extends XCellImpl  {
    public FloatCellImpl(Cell cell) {
      super(cell);
    }

    public FloatCellImpl value(double value) {
      cell.setCellValue(value);
      return this;
    }


    public FloatCellImpl style(FloatStyle style) {
      super.style(style);
      return this;
    }
  }
  
  public static class BooleanCellImpl extends XCellImpl  {
    public BooleanCellImpl(Cell cell) {
      super(cell);
    }

    public BooleanCellImpl value(boolean value) {
      cell.setCellValue(value);
      return this;
    }

    public BooleanCellImpl style(BooleanStyle style) {
      super.style(style);
      return this;
    }
  }
  
  public static class StringCellImpl extends XCellImpl  {
    public StringCellImpl(Cell cell) {
      super(cell);
    }

    public StringCellImpl value(String value) {
      cell.setCellValue(value);
      return this;
    }

    public StringCellImpl style(StringStyle style) {
      super.style(style);
      return this;
    }
  }

  
  public static class DateCellImpl extends XCellImpl  {
    public DateCellImpl(Cell cell) {
      super(cell);
    }

    public DateCellImpl value(Date value) {
      cell.setCellValue(value);
      return this;
    }

    public DateCellImpl value(Calendar value) {
      cell.setCellValue(value);
      return this;
    }

    public DateCellImpl style(DateStyle style) {
      super.style(style);
      return this;
    }
  }
  
  public static class TimestampCellImpl extends XCellImpl  {
    public TimestampCellImpl(Cell cell) {
      super(cell);
    }

    public TimestampCellImpl value(Date value) {
      cell.setCellValue(value);
      return this;
    }

    public TimestampCellImpl value(Calendar value) {
      cell.setCellValue(value);
      return this;
    }

    public TimestampCellImpl style(TimestampStyle style) {
      super.style(style);
      return this;
    }
  }
  
  
  public static class FormulaCellImpl extends XCellImpl  {
    public FormulaCellImpl(Cell cell) {
      super(cell);
    }
    public FormulaCellImpl value(String value) {
      cell.setCellFormula(value);
      return this;
    }
    public FormulaCellImpl style(FormulaStyle style) {
      super.style(style);
      return this;
    }    
  }

}