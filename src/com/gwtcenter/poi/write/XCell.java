package com.gwtcenter.poi.write;

/**
 * シート中に作成されるセルを表す。
 * @author ysugimura
 *
 * @param <T>
 */
public interface XCell {

  /**
   * このセルのセルスタイルを取得する。
   * <p> 
   * セルが作成された後で、セルスタイルを変更してもよい。
   * この場合、そのセルだけに影響し、同じセルスタイルの設定された他のセルには影響しない。
   * </p>
   * @return {@link XCellStyle}
   */
  XCellStyle getStyle();
}