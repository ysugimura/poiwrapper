package com.gwtcenter.poi.write;

import java.util.*;

import com.gwtcenter.poi.write.XFmtStyle.*;
import com.gwtcenter.poi.write.impl.*;

/**
 * エクセルシート
 * <p>
 * addメソッドによってセルを、左上から右方向に追加していく。
 * 現在位置に何も置きたく無い場合は{@link #skip()}を呼び出す。
 * 次の行に移りたい場合には{@link #nextRow()}を呼び出す。
 * 任意場所へのセル追加は、事前に{@link #position(int, int)}を呼び出しておく。
 * </p>
 * <p>
 * 返されたセルの属性やデータの設定は{@link XCellImpl}参照のこと。
 * </p>
 * @author ysugimura
 */
public interface XSheet {
  
  /** 現在位置を変更する */
  XSheet position(int row, int col);

  /** 次の行の左端に位置づける */
  void nextRow();
  
  /** 現在行の高さを設定する */
  void setRowHeight(float size);

  /** セルを一つ飛ばす。現在セル位置が一つ右にずれる。 */
  void skip();

  /** 現在位置に整数セルを追加する。 現在セル位置が一つ右にずれる。*/  
  XCell add(IntStyle style, long value);
  
  /** 現在位置に浮動小数点セルを追加する。 現在セル位置が一つ右にずれる。*/
  XCell add(FloatStyle style, double value);
  
  /** 現在位置に文字列セルを追加する。現在セル位置が一つ右にずれる。 */
  XCell add(StringStyle style, String vaue);
  
  /** 現在位置に日付セルを追加する。現在セル位置が一つ右にずれる。 */
  XCell add(DateStyle style, Date value);
  
  /** 現在位置に日付・時刻セルを追加する。現在セル位置が一つ右にずれる。 */
  XCell add(TimestampStyle style, Date value);
  
  /** 現在位置に式を追加する。 現在セル位置が一つ右にずれる。*/
  XCell add(FormulaStyle style, String value);
  
  /** 現在位置に論理値セルを追加する。 現在セル位置が一つ右にずれる。*/
  XCell add(BooleanStyle style, boolean value);

  /** 
   * 現在行にページブレークを置く。
   * つまり、現在行がその現在のページの最後の行になり、nextRow()後の行は次のページになる。
   */
  void setRowBreak();
 
  /** 
   * ヘッダとする行数を指定する。
   * これは表示上でも固定行となり、印刷上でも全ページに印刷される行となる。
   * @param rows
   * @return
   */
  public XSheet setHeader(int rows);

  /**
   * ヘッダとする行数、列数を指定する。
   * 表示上でも固定行・固定列となり、印刷上でも全ページに印刷される行・列となる。
   * @param rows
   * @param cols
   * @return
   */
  public XSheet setHeader(int rows, int cols);
  
  /** 指定列を自動サイズする */
  void autoSize(int...columns);
  
  /** 全列を自動サイズする */
  void autoSizeAll();
}
