package com.gwtcenter.poi.write;

/**
 * セルのフォーマット
 * <p>
 * セルフォーマットは、セルの値を表示する場合にどう表示されるかを決定する。
 * 例えば、同じ浮動小数点数でも、小数点以下二桁とゼロ桁の違いを作り出せる。
 * しかし、ここでは、Excel本来の仕様に沿った「あいまいな型」は使わず、もう少々明確な型をサポートしている。
 * 明示的に整数、浮動小数点数、日付のみ、日付＋時刻などである。
 * </p>
 * <p>
 * このため、各「フォーマット」は、それを使用可能な型が異なる。つまり、整数用のフォーマットは整数にしか使用できない。
 * フォーマットを作成する場合には、明示的にどの型用のものかを指定しなければならない。
 * </p>
 * <p>
 * 自由に作成できるフォーマットとは別に、Excel側で定義済のフォーマットがある。
 * これについても、ここでは適用できる型を明示している。
 * 定義済フォーマット番号については、{@link org.apache.poi.ss.usermodel.BuiltinFormats}を参照のこと。
 * </p>
 * @author ysugimura
 */
public class XFormat {

  // 定義済フォーマット
  
  /** 一般形式 */
  public static final StringFmt GENERAL = new StringFmt((short)0);
  
  /** 標準整数形式、 */
  public static final IntFmt INTEGER = new IntFmt((short)1);

  /** 少数2桁形式、"0.00" */
  public static final FloatFmt FLOAT = new FloatFmt((short)2);

  /** 三桁区切り整数形式、"#,##0" */
  public static final IntFmt THOUSANDS_INTEGER = new IntFmt((short)3);
  
  /** 三桁区切り少数2桁形式、"#,##0.00" */
  public static final FloatFmt THOUSANDS_FLOAT = new FloatFmt((short)4);
  
  /** Excelの内部でこのフォーマットを表す番号 */
  public final short number;

  @Override
  public String toString() {
    return XFormat.class.getSimpleName() + ":" + number;
  }
  
  XFormat(short number) {
    this.number = number;
  }

  public static class BooleanFmt extends XFormat {
    public BooleanFmt(short number) {
      super(number);
    }
  }
  
  public static class DateFmt extends XFormat {
    public DateFmt(short number) {
      super(number);
    }
  }
  
  public static class TimestampFmt extends XFormat {
    public TimestampFmt(short number) {
      super(number);
    }
  }
  
  public static class IntFmt extends XFormat {
    public IntFmt(short number) {
      super(number);
    }
  }
  
  public static class FloatFmt extends XFormat {
    public FloatFmt(short number) {
      super(number);
    }
  }
  
  public static class StringFmt extends XFormat {
    public  StringFmt(short number) {
      super(number);
    }
  }  
    
  public static class FormulaFmt extends XFormat {
    public FormulaFmt(short number) {
      super(number);
    }
  }
}
