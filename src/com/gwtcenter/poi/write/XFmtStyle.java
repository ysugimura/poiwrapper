package com.gwtcenter.poi.write;

/**
 * セルのスタイル
 * <p>
 * 単純に{@link XCellStyle}をラップしたもの。
 * {@link XCell}
 * </p>
 * @author ysugimura
 */
public abstract class XFmtStyle {
  
  public final XCellStyle cellStyle;
  
  XFmtStyle(XCellStyle cellStyle) {
    this.cellStyle = cellStyle;
  }

  @Override
  public String toString() {
    return XFmtStyle.class.getSimpleName() + ":" + cellStyle;
  }
  
  /** 日付用のセルスタイル */
  public static class DateStyle extends XFmtStyle {
    public DateStyle(XCellStyle cellStyle) {
      super(cellStyle);
    }
  }
  
  /** タイムスタンプ用のセルスタイル */
  public static class TimestampStyle extends XFmtStyle {
    public TimestampStyle(XCellStyle cellStyle) {
      super(cellStyle);
    }
  }

  /** 文字列用のセルスタイル */
  public static class StringStyle extends XFmtStyle  {
    public StringStyle(XCellStyle cellStyle) {
      super(cellStyle);
    }
  }

  public static class IntStyle extends XFmtStyle {
    public IntStyle(XCellStyle cellStyle) {
      super(cellStyle);
    }
  }
  
  /** 数値用のセルスタイル */
  public static class FloatStyle extends XFmtStyle {
    public FloatStyle(XCellStyle cellStyle) {
      super(cellStyle);
    }
  }
  
  /** 論理値用のセルスタイル */
  public static class BooleanStyle extends XFmtStyle {    
    public BooleanStyle(XCellStyle cellStyle) {
      super(cellStyle);
    }
  }
  
  /** 式用のセルスタイル */
  public static class FormulaStyle extends XFmtStyle {    
    public FormulaStyle(XCellStyle cellStyle) {
      super(cellStyle);
    }
  }
}
