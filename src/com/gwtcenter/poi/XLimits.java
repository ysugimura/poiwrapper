package com.gwtcenter.poi;

public enum XLimits {

  /**
   * Excel97 format aka BIFF8
   * <ul>
   * <li>The total number of available rows is 64k (2^16)</li>
   * <li>The total number of available columns is 256 (2^8)</li>
   * <li>The maximum number of arguments to a function is 30</li>
   * <li>Number of conditional format conditions on a cell is 3</li>
   * <li>Number of cell styles is 4000</li>
   * <li>Length of text cell contents is 32767</li>
   * </ul>
   */
  EXCEL97(0x10000, 0x0100, 30, 3, 4000, 32767),

  /**
   * Excel2007
   *
   * <ul>
   * <li>The total number of available rows is 1M (2^20)</li>
   * <li>The total number of available columns is 16K (2^14)</li>
   * <li>The maximum number of arguments to a function is 255</li>
   * <li>Number of conditional format conditions on a cell is unlimited
   * (actually limited by available memory in Excel)</li>
   * <li>Number of cell styles is 64000</li>
   * <li>Length of text cell contents is 32767</li>
   * <ul>
   */
  EXCEL2007(0x100000, 0x4000, 255, Integer.MAX_VALUE, 64000, 32767);

  /** 最大行数 */
  public final int maxRows;
  
  /** 最大列数 */
  public final int maxColumns;
  
  /** 最大関数引数数 */
  public final int maxFunctionArgs;
  
  /** セル当たりのフォーマット数？ */
  public final int maxCondFormats;
  
  /** セルスタイル最大数 */
  public final int maxCellStyles;
  
  /** 最大テキスト長さ */
  public final int maxTextLength;

  private XLimits(int maxRows, int maxColumns, int maxFunctionArgs, int maxCondFormats, int maxCellStyles, int maxText) {
      this.maxRows = maxRows;
      this.maxColumns = maxColumns;
      this.maxFunctionArgs = maxFunctionArgs;
      this.maxCondFormats = maxCondFormats;
      this.maxCellStyles = maxCellStyles;
      this.maxTextLength = maxText;
  }
}
