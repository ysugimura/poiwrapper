
import java.io.*;
import java.nio.file.*;
import java.util.*;

import com.gwtcenter.poi.*;
import com.gwtcenter.poi.write.*;
import com.gwtcenter.poi.write.XFmtStyle.*;
import com.gwtcenter.poi.write.XFormat.*;
import com.gwtcenter.poi.write.impl.*;

public class MainTest {
  public static void main(String[] args) throws Exception {

    /*
    try (SXSSFWorkbook workbook = new SXSSFWorkbook()) {

      DataFormat dataFormat = workbook.createDataFormat();
      dataFormat.getFormat("yy/MM/dd");
      short format = dataFormat.getFormat("yyyy/MM/dd");
      CellStyle center = workbook.createCellStyle();
      center.setDataFormat(format);

      Sheet sheet = workbook.createSheet("foobar");

      Row row0 = sheet.createRow((short) 0);
      Cell cell0 = row0.createCell((short) 0);
      cell0.setCellStyle(center);
      cell0.setCellValue(new Date());


      try (OutputStream out = Files.newOutputStream(Paths.get("C:\\Users\\admin\\Desktop\\out.xlsx"))) {
        workbook.write(out);
      }
    }
    */
    
    try (XBook book = XBookCreator.create(XType.XLSX)) {
      DateFmt f = book.dateFormat("yyyy/MM/dd");
      XCellStyle cellStyle = book.createCellStyle();
      DateStyle dateStyle = cellStyle.dateStyle(f);
      XSheet sheet = book.createSheet("test");
      sheet.add(dateStyle, new Date());
      try (OutputStream out = Files.newOutputStream(Paths.get("C:\\Users\\admin\\Desktop\\out.xlsx"))) {
        book.write(out);
      }
    }
  }
}