import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

import com.gwtcenter.poi.*;
import com.gwtcenter.poi.read.*;
import com.gwtcenter.poi.read.impl.*;

public class ReadTest2 {
  
  public static void main(String[]args) throws IOException {
    Path path = Paths.get("C:\\Users\\admin\\Desktop\\test.xlsx");
    System.out.println("Reading " + path);
    
    try (XReader reader = XReaderCreator.create(XType.XLSX, path)) {
      String[][]cells = reader.read(0, new XToString());
      
      
      Arrays.stream(cells).forEach(row->
        System.out.println(Arrays.stream(row).collect(Collectors.joining(",")))
      );
    }          
  }
}
