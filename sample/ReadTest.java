import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

import com.gwtcenter.poi.*;
import com.gwtcenter.poi.read.*;
import com.gwtcenter.poi.read.impl.*;

public class ReadTest {
  
  public static void main(String[]args) throws IOException {
    read(XType.XLSX);
    read(XType.XLS);
  }
  
  private static void read(XType xType) throws IOException {
    Path path = Consts.getExcelFile(xType);
    System.out.println("Reading " + path);
    try (XReader reader = XReaderCreator.create(xType, path)) {
      String[][]cells = reader.read(0, new XToString());
      Arrays.stream(cells).forEach(row->
        System.out.println(Arrays.stream(row).collect(Collectors.joining(",")))
      );
    }          
  }
}
