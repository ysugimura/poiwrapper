import java.nio.file.*;

import com.gwtcenter.poi.*;

public class Consts {

  static final Path FOLDER = Paths.get("C:\\Users\\admin\\Desktop");

  static final Path getExcelFile(XType xType) {
    return FOLDER.resolve("out." + xType.extension);
  }
  
  private static int tempNumber;
  
  static final Path getTempFile()  {
    return FOLDER.resolve("temp-" + tempNumber++);
  }
      
}
