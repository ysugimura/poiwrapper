import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

import com.gwtcenter.poi.*;
import com.gwtcenter.poi.write.*;
import com.gwtcenter.poi.write.XFmtStyle.*;
import com.gwtcenter.poi.write.impl.*;

public class WriteTest {
  
  public static void main(String[] args) throws IOException {    
    XBookCreator.setTempFileSupplier(()->Consts.getTempFile());    
    create(XType.XLSX);
    create(XType.XLS);
  }
  
  private static void create(XType xType) throws IOException {
    try (XBook book = XBookCreator.create(xType)) {
      
      XFont mincho = book.createFontBuilder().setName("MS 明朝").setSize(20).build();
      XFont gothic = book.createFontBuilder().setName("MS ゴシック").setSize(18).build();

      XCellStyle cellStyle = book.createCellStyle().setThinBorders().setFont(mincho);
      
      StringStyle titleStyle = cellStyle.stringStyle(XFormat.GENERAL);
      DateStyle dateStyle = cellStyle.dateStyle(book.dateFormat("yyyy/MM/dd"));
      TimestampStyle timestampStyle = cellStyle.timestampStyle(book.timestampFormat("yyyy/MM/dd HH:mm:ss"));
      IntStyle intStyle = cellStyle.intStyle(XFormat.THOUSANDS_INTEGER);
      FloatStyle floatStyle = cellStyle.floatStyle(XFormat.THOUSANDS_FLOAT);      
      
      XSheet sheet = book.createSheet("シート1");

      sheet.skip();
      IntStream.range(0, 5).forEach(x->sheet.add(titleStyle, "列タイトル " + x));
      sheet.setRowHeight(20);
      sheet.nextRow();
      IntStream.range(0, 10).forEach(y-> {
        sheet.add(titleStyle, "行タイトル " + y).getStyle().setFont(gothic);
        sheet.add(timestampStyle, new Date());        
        sheet.add(dateStyle, new Date());
        
        XCell cell = sheet.add(titleStyle,  "寿限無じゅげむ");
        if (y == 5) cell.getStyle().setFont(gothic).setBgColor(XColor.AQUA);
        
        sheet.add(intStyle, y * 12345);
        sheet.add(floatStyle, y * 12345).getStyle().setFont(gothic);
        sheet.setRowHeight(20);
        sheet.nextRow();
      });
      
      sheet.setHeader(1, 1);   
      sheet.autoSizeAll();
      book.write(Files.newOutputStream(Consts.getExcelFile(xType)));
    }
  }
}
